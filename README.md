# Automation-Task

Work was done on Windows 10 Home Release 1809

## Getting Started, Short Guide.

Clone repo
Install required npm packages

```
npm install
```
Update webdriver-manager
```
./node_modules/protractor/bin/webdriver-manager update
```
For MicrosoftEdge usage install webdriver for Edge
Open PowerShell as admin
```
DISM.exe /Online /Add-Capability /CapabilityName:Microsoft.WebDriver~~~~0.0.1.0
```
To launch protractor
```
$ ./node_modules/protractor/bin/protractor automation.conf.js
```
After test is completed additional folders would be created for results:

/html - *.html file  
/results - *.xml file  
In case of error, screenshot will be visible on html report.

By default protractor will use chrome. To change browser edit automation.conf.js

```
capabilities: {
  'browserName': 'chrome' // chrome, firefox, MicrosoftEdge
},
```
### Short file explanation

automation.conf.js - protractor configuration file  
database.xlsx - workers data  
extensions.js - additional helpers used it test  
hrmangmente.po.js - Orange Hr page objects   
hrmangment.spec.js - Test file  
myDom.po.js - additional helper to easier find elements
