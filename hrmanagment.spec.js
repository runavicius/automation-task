var hrmanagmentJS = require('./hrmanagment.po.js');

describe('HR Managment automation', function() {
  var hrPO = new hrmanagmentJS();

  it('Add employee, assign him to vacation', function() {
    flow.execute(function() {
      var employees = require("./employees.json");
      var employers = require("./employers.json");
      hrPO.login('https://opensource-demo.orangehrmlive.com/', employers[0].Login, employers[0].Pass);
      browser.wait(EC.presenceOf(hrPO.PIM), 5000);
      hrPO.PIM.waitClick(); hrPO.PIM.waitClick();
      browser.wait(EC.presenceOf(hrPO.addEmpl), 5000);
      hrPO.addEmpl.waitClick();
      hrPO.firstName.setValue(employees[0].Name);
      hrPO.lastName.setValue(employees[0]["Last Name"]);
      hrPO.emplId.setValue(employees[0]["Employee ID"]);
      hrPO.createLoginDetails.isSelected().then(function(checked) {
        if (!checked) {
          hrPO.createLoginDetails.waitClick();
        }
      });
      hrPO.emplUserName.setValue(employees[0].Login);
      hrPO.emplPass.setValue(employees[0].Pass);
      hrPO.confirmEmplPass.setValue(employees[0].Pass);
      hrPO.saveBtn.waitClick();
      hrPO.editBtn.waitClick();
      hrPO.gender(employees[0].Gender).waitClick();
      hrPO.nationality.selectOption(employees[0].Nationality);
      hrPO.maritalStatus.selectOption(employees[0]["Marital Status"]);
      hrPO.birthDate.setValue(employees[0].Birth);
      hrPO.saveBtn.waitClick();
      hrPO.contactDetails.waitClick();
      hrPO.editBtn.waitClick();
      hrPO.address1.setValue(employees[0].Address.street);
      hrPO.city.setValue(employees[0].Address.city);
      hrPO.country.selectOption(employees[0].Address.country);
      hrPO.homeTel.setValue(employees[0].Phone.home);
      hrPO.mobileTel.setValue(employees[0].Phone.mobile);
      hrPO.workEmail.setValue(employees[0].Email.work);
      hrPO.otherEmail.setValue(employees[0].Email.home);
      hrPO.saveBtn.waitClick();
      hrPO.job.waitClick();
      hrPO.editBtn.waitClick();
      hrPO.jobTitle.selectOption(employees[0].Job.title);
      hrPO.jobCategory.selectOption(employees[0].Job.category);
      hrPO.emplStatus.selectOption(employees[0].Job.status);
      hrPO.subUnit.selectOption(employees[0].Job.SubUnit);
      hrPO.joinedDate.setValue(employees[0].Joined);
      hrPO.saveBtn.waitClick();
      hrPO.leave.waitClick();
      hrPO.assignLeave.waitClick();
      hrPO.emplName.setValue(employees[0].Name + ' ' + employees[0]["Last Name"]);
      hrPO.leaveType.selectOption('Vacation US');
      hrPO.leaveStart.setValue(employees[0].Vacation.Start);
      hrPO.leaveEnd.setValue(employees[0].Vacation.End);
      hrPO.comment.setValue('Holidays');
      hrPO.assign.waitClick();
      hrPO.leaveConfirmModal.isDisplayed().then(function (isDisplayed) {
        if (isDisplayed) {
          browser.wait(EC.visibilityOf(hrPO.okBtn), 5000)
          hrPO.okBtn.waitClick();
          browser.sleep(2000);
        }
      });
      hrPO.userPanel.waitClick();
      browser.wait(EC.visibilityOf(hrPO.logout), 5000);
      hrPO.logout.waitClick();
      hrPO.username.setValue(employees[0].Login);
      hrPO.password.setValue(employees[0].Pass);
      hrPO.loginButton.waitClick();
      hrPO.leave.waitClick();
      hrPO.myLeave.waitClick();
      expect(hrPO.dataTable.count()).toBe(1);
      expect(hrPO.tableVacationEnd.getText()).toContain(employees[0].Vacation.End);
    });
  });
});
