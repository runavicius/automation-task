var onPrepare = function() {
  convertExcel = require('excel-as-json').processFile
  options ={
      sheet: '1',
      omitEmtpyFields: false
    };
  convertExcel('./database.xlsx', 'employees.json', options);
  options ={
      sheet: '2',
      omitEmtpyFields: false
    };
  convertExcel('./database.xlsx', 'employers.json', options);
  browser.sleep(5000);
  browser.ignoreSynchronization = true;
  browser.driver.manage().window().maximize();

  require('./extensions.js').setup();
  EC = protractor.ExpectedConditions;
  flow = protractor.promise.controlFlow();
  var fs = require('fs-extra');

  fs.emptyDir('./html/screenshots/', function(err) {
    console.log(err);
  });

  jasmine.getEnv().addReporter({
    specDone: function(result) {
      if (result.status == 'failed') {
        browser.getCapabilities().then(function(caps) {
          var browserName = caps.get('browserName');

          browser.takeScreenshot().then(function(png) {
            var stream = fs.createWriteStream('./html/screenshots/' + browserName + '-' + browserName + '.' + result.fullName + '.png');
            stream.write(new Buffer(png, 'base64'));
            stream.end();
          });
        });
      }
    }
  });

  return browser.getCapabilities().then(function(capabilities) {
    var browserName = capabilities.get('browserName');
    var jasmineReporters = require('jasmine-reporters');
    var junitReporter = new jasmineReporters.JUnitXmlReporter({
      consolidateAll: true,
      savePath: 'results/',
      filePrefix: browserName + '-results',
      modifySuiteName: function(generatedSuiteName, suite) {
        return browserName + '.' + generatedSuiteName;
      },
    });
    jasmine.getEnv().addReporter(junitReporter);
  });
};

exports.config = {
  onPrepare: onPrepare,
  seleniumAddress: undefined,
  allScriptsTimeout: 2500000,
  getPageTimeout: 250000,
  jasmineNodeOpts: {
    defaultTimeoutInterval: 2500000
  },
  framework: 'jasmine',
  suites: {
    orangeHR: 'hrmanagment.spec.js'
  },

  capabilities: {
    'browserName': 'chrome'
  },
   //If multiCapabilities is used, same test is running parrel
   //Issue what we add same user, we cannot add same user at same time
   //In multiple browsers.
   //For Edge we need to start webdriver manually and use it address

//   multiCapabilities: [{
//     browserName: "chrome",
//
// },  {
//     browserName: "firefox",
//
// },  {
//     browserName: "MicrosoftEdge",
// }],

onComplete: function() {
  var HTMLReport = require('protractor-html-reporter-2');
  var browserName, browserVersion;
  var capsPromise = browser.getCapabilities();

  capsPromise.then(function(caps) {
    browserName = caps.get('browserName');
    browserVersion = caps.get('version');
    platform = caps.get('platform');

    var HTMLReport = require('protractor-html-reporter-2');

    testConfig = {
      reportTitle: 'HR managment',
      outputPath: './html',
      outputFilename: 'report',
      screenshotPath: './screenshots',
      testBrowser: browserName,
      browserVersion: browserVersion,
      modifiedSuiteName: false,
      screenshotsOnlyOnFailure: true,
      testPlatform: platform
    };
    new HTMLReport().from('./results/' + browserName + '-results.xml', testConfig);
  });
},
};
