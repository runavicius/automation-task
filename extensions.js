function setup() {

  protractor.ElementFinder.prototype.setValue = function(value) {
    self = this;
    browser.driver.wait(EC.elementToBeClickable(self), 10000);
    return self.clear().sendKeys(value);
  };
  protractor.ElementFinder.prototype.waitFor = function(millis) {
    millis = millis ? millis : 5000;
    browser.wait(EC.presenceOf(this), millis);
  };
  protractor.ElementFinder.prototype.waitClick = function(waitTime) {
    waitTime = waitTime ? waitTime : 11500;
    self = this;
    browser.driver.wait(EC.presenceOf(self), waitTime);
    browser.sleep(200);
    self.click();
  };
  protractor.ElementFinder.prototype.selectOption = function(optionText) {
    self = this;
    browser.driver.wait(EC.presenceOf(self), 5000);
    self.element(by.cssContainingText('option', optionText)).click();
  };
}

exports.setup = setup;
