var hrmanagmentPo = function() {
  var myDomJSPO = require('./myDom.po.js');
  var myDomJS = new myDomJSPO();
// Login Page
this.username = element(by.id('txtUsername'));
this.password = element(by.id('txtPassword'));
this.loginButton = element(by.id('btnLogin'));
// PIM
this.PIM = element(by.id('menu_pim_viewPimModule'));
this.addEmpl = element(by.id('menu_pim_addEmployee'));
this.firstName = element(by.id('firstName'));
this.middleName = element(by.id('middleName'));
this.lastName = element(by.id('lastName'));
this.emplId = myDomJS.elementByNearLabel('Employee Id','input');
this.createLoginDetails = element(by.id('chkLogin'));
this.emplUserName = myDomJS.elementByNearLabel('User Name','input');
this.emplPass = myDomJS.elementByNearLabel('Password','input');
this.confirmEmplPass = myDomJS.elementByNearLabel('Confirm Password','input');
this.saveBtn = element(by.buttonText('Save'));
// Leave
this.leave = element(by.cssContainingText('b', 'Leave'));
this.assignLeave = element(by.id('menu_leave_assignLeave'));
this.emplName = myDomJS.elementByNearLabel('Employee Name','input');
this.leaveType = myDomJS.elementByNearLabel('Leave Type','select');
this.leaveStart = myDomJS.elementByNearLabel('From Date', 'input');
this.leaveEnd = myDomJS.elementByNearLabel('To Date', 'input');
this.comment = element(by.id('assignleave_txtComment'));
this.assign = element(by.id('assignBtn'));
// Personal details
this.gender = function(gender) {
  return element(by.cssContainingText('label', gender));
};
this.nationality = myDomJS.elementByNearLabel('Nationality', 'select');
this.maritalStatus = myDomJS.elementByNearLabel('Marital Status', 'select');
this.birthDate = myDomJS.elementByNearLabel('Date of Birth', 'input');
//Contact details
this.contactDetails = element(by.cssContainingText('a', 'Contact Details'));
this.address1 = myDomJS.elementByNearLabel('Address Street 1' , 'input');
this.address2 = myDomJS.elementByNearLabel('Address Street 2' , 'input');
this.city = myDomJS.elementByNearLabel('City' , 'input');
this.state = myDomJS.elementByNearLabel('State/Province' , 'input');
this.postalCode = myDomJS.elementByNearLabel('Zip/Postal Code' , 'input');
this.country = myDomJS.elementByNearLabel('Country', 'select');
this.homeTel = myDomJS.elementByNearLabel('Home Telephone', 'input');
this.mobileTel = myDomJS.elementByNearLabel('Mobile', 'input');
this.workTel = myDomJS.elementByNearLabel('Work Telephone', 'input');
this.workEmail = myDomJS.elementByNearLabel('Work Email', 'input');
this.otherEmail = myDomJS.elementByNearLabel('Other Email', 'input');
//Job
this.job = element(by.css('a[href*="viewJobDetails"]'));
this.jobTitle = myDomJS.elementByNearLabel('Job Title', 'select');
this.emplStatus = myDomJS.elementByNearLabel('Employment Status', 'select');
this.jobCategory = myDomJS.elementByNearLabel('Job Category', 'select');
this.subUnit = myDomJS.elementByNearLabel('Sub Unit', 'select');
this.location = myDomJS.elementByNearLabel('Location', 'select');
this.startDate = myDomJS.elementByNearLabel('Start Date', 'input');
this.joinedDate = myDomJS.elementByNearLabel('Joined Date', 'input');

this.editBtn = element.all(by.buttonText('Edit')).get(0);

this.userPanel = element(by.cssContainingText('a', 'Welcome Admin'));
var userMenu = element(by.id('welcome-menu'));
this.logout = userMenu.element(by.cssContainingText('a', 'Logout'));

this.leaveConfirmModal = element(by.id('leaveBalanceConfirm'));
this.okBtn = element(by.id('confirmOkButton'));
this.leave = element(by.cssContainingText('b', 'Leave'));

this.myLeave = element(by.css('a', 'My Leave'));

var leaveTable = element(by.id('resultTable')).element(by.css('tbody'));
this.dataTable = leaveTable.all(by.css('tr'));
this.tableVacationEnd = this.dataTable.all(by.css('td')).get(0);

this.login = function(host, user, pass) {
  browser.get(host);
  browser.wait(EC.visibilityOf(this.username), 15000, 'no username field');
  browser.wait(EC.visibilityOf(this.password), 15000, 'no password field');
  browser.wait(EC.elementToBeClickable(this.loginButton), 15000)
  this.username.setValue(user);
  this.password.setValue(pass);
  this.loginButton.waitClick();
};
}
module.exports = hrmanagmentPo;
