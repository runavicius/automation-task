var myDom = function() {
  //element finder extension
  this.elementByNearLabel = function(labelString, adjacentTag) {
    return element(by.cssContainingText('label', labelString)).element(by.xpath('..')).element(by.css(adjacentTag));
  };
};

module.exports = myDom;
